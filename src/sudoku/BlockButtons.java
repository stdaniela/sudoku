package sudoku;

import javax.swing.JToggleButton;

public class BlockButtons {
    public BlockButtons(JToggleButton[][] tb) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                tb[i][j].setEnabled(false);
            }
        }
    }
}
