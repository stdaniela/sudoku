package sudoku;

import javax.swing.JToggleButton;

public class Line {
    public boolean verify(JToggleButton[][] tb) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 8; j++) {
                for (int k = j+1; k < 9; k++) {
                    if ( tb[i][j].getText().equals(tb[i][k].getText()) ) {
                        return false;
                    }
                    if ( tb[j][i].getText().equals(tb[k][i].getText()) ) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
