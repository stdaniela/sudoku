package sudoku;

import java.awt.Color;
import java.io.*;
import javax.swing.*;

public class NewSudokuFromFile {
    public NewSudokuFromFile(JToggleButton[][]t, String filename) {
        int i = 0, j = 0;
        try {
            File file = new File(filename);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            
            while (line != null) {
                for (char ch : line.toCharArray()) {
                    if (ch != ',') {
                        if (ch == '0') {
                            t[i][j].setText(" ");
                            t[i][j].setEnabled(true);
                            t[i][j].setForeground(Color.BLUE);
                        } else {
                            t[i][j].setText(ch + "");
                            t[i][j].setEnabled(false);
                            t[i][j].setForeground(Color.GREEN);
                        }
                        j++;
                        if (j >= 9) {
                            i++;
                            j = 0;
                        }
                    }
                }
                line = br.readLine();
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
