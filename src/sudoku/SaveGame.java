package sudoku;

import java.io.*;
import javax.swing.JToggleButton;

public class SaveGame {
    public SaveGame(JToggleButton[][] t, int level, int number) {
        try {
            File file = new File("sudoku" + File.separator + "sudoku.txt");
            PrintWriter pw = new PrintWriter(file);
            
            pw.println(level);
            pw.println(number);
            
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (t[i][j].getText().equals(" ")) {
                        pw.print("0");
                    } else {
                        pw.print(t[i][j].getText());
                    }
                    if (j != 8) {
                        pw.print(",");
                    }
                }
                pw.println();
            }
            pw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
