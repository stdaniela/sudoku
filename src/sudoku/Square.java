package sudoku;

import javax.swing.JToggleButton;

public class Square {
    public boolean verify(JToggleButton[][] tb) {
        //pe i si j tb sa ii maresc din 3 in 3 si fac verif pe 
        for (int i = 0; i < 9; i+=3) {
            for (int j = 0; j < 9; j+=3) {
                for (int k = i; k < i + 3; k++) {
                    for (int l = j; l < j + 3; l++) {
                        if ( ( (k != i) || (l != j) ) &&
                                (tb[k][l].getText().equals(tb[i][j].getText())) ) {
                            return false;
                        }
                    }
                }
            }
        }      
        return true;
    }
}
