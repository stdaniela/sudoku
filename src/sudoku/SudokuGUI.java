package sudoku;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

public class SudokuGUI {
    private JFrame frame = new JFrame("Sudoku");
    private JToggleButton[][] toggleButtons1;
    private JToggleButton[] toggleButtons2;
    
    private int currentNumber;
    private int sudokuLevel; // nivelul curent
    private int sudokuNumber; // numarul jocului
    
    public SudokuGUI(int level, int number) {
        frame.setSize(550,520);
        frame.setResizable(false);
        setLookAndFeel();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        BorderLayout borderLayout = new BorderLayout();
        frame.setLayout(borderLayout);
        
        setToggleButtons1();
        setToggleButtons2();
        setButtons();
        
        sudokuLevel = level;
        sudokuNumber = number;
        String filename = "sudoku" + File.separator + "Level " + 
                sudokuLevel + File.separator + "sudoku" + sudokuNumber + ".txt";
        new NewSudokuFromFile(toggleButtons1, filename);
        
        frame.setVisible(true);
    }
    
    private void setToggleButtons1() {
        toggleButtons1 = new JToggleButton[9][9];
        ButtonGroup toggleButtons1Group = new ButtonGroup();
        JPanel panel = new JPanel();
        JPanel[][] panel1 = new JPanel[3][3];
        GridLayout gridLayout = new GridLayout(3,3,2,2);
        panel.setLayout(gridLayout);
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                panel1[i][j] = new JPanel();
                panel1[i][j].setLayout(gridLayout);
                panel1[i][j].setBorder(BorderFactory.createEtchedBorder());
                panel.add(panel1[i][j]);
            }
        }
        
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                toggleButtons1[i][j] = new JToggleButton();
                toggleButtons1[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JToggleButton tb = (JToggleButton)e.getSource();
                        if (tb.getText().equals(" ")) {
                            tb.setText(currentNumber + "");
                        } else {
                            tb.setText(" ");
                        }
                        
                        if (fullTable()) {
                            Line line = new Line();
                            Square square = new Square();
                            if (line.verify(toggleButtons1) && square.verify(toggleButtons1)) {
                                new BlockButtons(toggleButtons1);
                                ImageIcon icon = new ImageIcon("icons" + File.separator + "prize_winner.png");
                                JOptionPane.showMessageDialog(frame, "Felicitari!", "Game Over", JOptionPane.INFORMATION_MESSAGE,icon);
                            } else {
                                JOptionPane.showMessageDialog(frame, "Mai incearca!", "Game Over", JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    }
                    
                    private boolean fullTable() {
                        for (int i = 0; i < 9; i++) {
                            for (int j = 0; j < 9; j++) {
                                if ( toggleButtons1[i][j].getText().equals(" ") ) {
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                });
                toggleButtons1[i][j].setFont(new Font("Arial", Font.BOLD, 18));
                panel1[i/3][j/3].add(toggleButtons1[i][j]);
                toggleButtons1Group.add(toggleButtons1[i][j]);
            }
        }
        
        frame.add(panel,BorderLayout.CENTER);
    }
    
    private void setToggleButtons2() {
        toggleButtons2 = new JToggleButton[9];
        ButtonGroup toggleButtons2Group = new ButtonGroup();
        GridLayout gridLayout = new GridLayout(1,9,3,3);
        JPanel panel = new JPanel();
        panel.setLayout(gridLayout);
        
        for (int i = 0; i < 9; i++) {
            toggleButtons2[i] = new JToggleButton();
            toggleButtons2[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JToggleButton tb = (JToggleButton)e.getSource();
                    currentNumber = Integer.parseInt(tb.getText());
                }
            });
            toggleButtons2[i].setFont(new Font("Arial", Font.BOLD, 18));
            toggleButtons2[i].setForeground(Color.MAGENTA);
            toggleButtons2[i].setText( (i + 1) + "");
            toggleButtons2Group.add(toggleButtons2[i]);
            panel.add(toggleButtons2[i]);
        }
        toggleButtons2[0].setSelected(true);
        currentNumber = 1;
        
        frame.add(panel,BorderLayout.NORTH);
    }
    
    private void setButtons() {
        JPanel buttonsPanel = new JPanel();
        GridLayout buttonsGridLayout = new GridLayout(2,3,5,5);
        buttonsPanel.setLayout(buttonsGridLayout);
        
        JButton newButton = new JButton("NEW", new ImageIcon("icons" + File.separator + "new24.png"));
        JButton resetButton = new JButton("RESET", new ImageIcon("icons" + File.separator + "reset24.png"));
        JButton saveButton = new JButton("SAVE", new ImageIcon("icons" + File.separator + "save24.png"));
        JButton loadButton = new JButton("LOAD", new ImageIcon("icons" + File.separator + "load24.png"));
        JButton levelButton = new JButton("LEVEL", new ImageIcon("icons" + File.separator + "level24.png"));
        JButton exitButton = new JButton("EXIT", new ImageIcon("icons" + File.separator + "exit24.png"));
        
        newButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(frame, "Are you sure that you want to start a new game?\n" +
                        "The current game will be lost unless you save it!", "New game", 
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (option == 0) {
                    sudokuNumber = new SudokuNumber().generate(sudokuLevel);
                    String filename = "sudoku" + File.separator + "Level " + 
                            sudokuLevel + File.separator + "sudoku" + sudokuNumber + ".txt";
                    new NewSudokuFromFile(toggleButtons1, filename);
                }
            }
        });
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(frame, "Are you sure that you want to reset this game?\n" +
                        "The current progress will be lost unless you save it!", "Reset game", 
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (option == 0) {
                    String filename = "sudoku" + File.separator + "Level " + 
                            sudokuLevel + File.separator + "sudoku" + sudokuNumber + ".txt";
                    new NewSudokuFromFile(toggleButtons1, filename);
                }
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(frame, "Are you sure that you want to save this game?\n" +
                        "The previous game will be lost!", "Save game", 
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (option == 0) {
                    new SaveGame(toggleButtons1, sudokuLevel, sudokuNumber);
                }
            }
        });
        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(frame, "Are you sure that you want to load the saved game?\n" +
                        "The current progress will be lost!", "Load game", 
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (option == 0) {
                    try {
                        File file = new File("sudoku" + File.separator + "sudoku.txt");
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        int savedLevel = Integer.parseInt(br.readLine());
                        int savedNumber = Integer.parseInt(br.readLine());
                        String filename = "sudoku" + File.separator + "Level " + savedLevel +
                                File.separator + "sudoku" + savedNumber + ".txt";
                        new NewSudokuFromFile(toggleButtons1, filename);

                        int i=0;
                        int j=0;
                        String line = br.readLine();
                        while (line != null) {
                            for (char ch : line.toCharArray()) {
                                if (ch != ',') {
                                    if (ch != '0') {
                                        toggleButtons1[i][j].setText(ch + "");
                                    }
                                    j++;
                                    if (j >= 9) {
                                        i++;
                                        j = 0;
                                    }
                                }
                            }
                            line = br.readLine();
                        }
                    
                    
                    } catch (Exception exc) {
                        JOptionPane.showMessageDialog(frame, "Inca nu ati salvat nici un joc",
                                "Eroare", JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });
        levelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ImageIcon icon = new ImageIcon("icons" + File.separator + "report.png");
                Object[] options = {"Level 1", "Level 2", "Level 3", "Level 4"};
                String stringLevel = (String)JOptionPane.showInputDialog(frame,
                        "Choose level", "Level", JOptionPane.QUESTION_MESSAGE, icon, options, options[sudokuLevel-1]);
                if (stringLevel != null) {
                    sudokuLevel = Integer.parseInt(stringLevel.charAt(6) + "");
                }
            }
        });
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            }
        });
        
        buttonsPanel.add(newButton);
        buttonsPanel.add(resetButton);
        buttonsPanel.add(saveButton);
        buttonsPanel.add(loadButton);
        buttonsPanel.add(levelButton);
        buttonsPanel.add(exitButton);
        
        frame.add(buttonsPanel, BorderLayout.SOUTH);
    }
    
    private void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(
                "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"
            );
        } catch (Exception exc) {
            // ignore error
        }
    }
}
