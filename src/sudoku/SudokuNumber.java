package sudoku;

public class SudokuNumber {
    public int generate(int level) {
        switch (level) {
            case 1:
                return (int)(Math.random()*8+1);
            case 2:
                return 30;
            case 3:
                return (int)(Math.random()*16 + 50);
            case 4:
                return (int)(Math.random()*8 + 100);
            default:
                return 0;
        }
    }
}
